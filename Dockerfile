FROM registry.gitlab.com/fredericmalo/appsec/cxsca-resolver-ubuntu:latest

#RUN service ntp restart
RUN apt-get -q update \
    && apt-get -q upgrade -y \
    && apt-get -q install -y zip \
    && apt-get -q clean


# MAVEN
# -----------------
RUN apt-get -q update -y  \
    && apt-get -q clean \
    && wget https://github.com/AdoptOpenJDK/openjdk8-binaries/releases/download/jdk8u265-b01/OpenJDK8U-jdk_x64_linux_hotspot_8u265b01.tar.gz -O openjdk8.tar.gz \
    && echo "1285da6278f2d38a790a21148d7e683f20de0799c44b937043830ef6b57f58c4 openjdk8.tar.gz" | sha256sum -c --status \
    && (mkdir /usr/lib/jvm/java-8-openjdk-amd64 || true) && tar -C /usr/lib/jvm/java-8-openjdk-amd64 --strip-components 1 -xzf openjdk8.tar.gz

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
ENV JAVA_HOME_8=/usr/lib/jvm/java-8-openjdk-amd64

RUN echo "**** JAVA_HOME ****"
RUN echo "$JAVA_HOME"
RUN mvn --version

